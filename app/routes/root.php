<?php

$app->get('/', function() use($app) {
  $datos = array(
    'titulo' => '/',
    'recursos' => array(
      array('url' => '/api', 'descripcion' => 'Este documento HTML'),
      array('url' => '/api/valor', 'descripcion' => 'Un documento XML'),
      array('url' => '/api/valor/id', 'descripcion' => 'Un documento XML'),
      array('url' => '/api/fecha', 'descripcion' => 'Un documento XML'),
      array('url' => '/api/fecha/id', 'descripcion' => 'Un documento XML'),
      array('url' => '/api/nota', 'descripcion' => 'Un documento XML'),
      array('url' => '/api/nota/id', 'descripcion' => 'Un documento XML'),
    )
  );
  $app->render('root.php', $datos);
});
