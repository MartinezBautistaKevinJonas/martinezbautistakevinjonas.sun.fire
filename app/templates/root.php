<!DOCTYPE html>
<html>
  <head><title><?php echo
 $titulo; ?></title><meta charset="UTF-8">
</head>
  <body onload="Aplicacion.main();">
    <h1>Recursos</h1>
    <h2>Encuesta Nacional de Ocupación y Empleo</h2>
    <p>
     Lista de recursos encontrados en la API, clic en el enlace para obtener el documento XML
    </p>
    <ol>
<?php foreach ($recursos as $r) { ?>
      <li><a href="<?php echo $r['url']; ?>"><?php echo $r['url']; ?></a>: <?php echo $r['descripcion']; ?></li>
<?php } ?>
    </ol>
    <hr />
    <p>
     Tabla del recurso Valor
    </p> 
<div class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="#"><?php echo $titulo; ?></a>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav"><li class="active"><a href="/">Books</a></li></ul>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div id="ventanaFormulario" class="col-md-12"></div>
    </div>
    <div class="row">
      <div id="ventanaMensaje" class="col-md-12"></div>
    </div>
    <div class="row">
      <div id="ventanaTabla" class="col-md-12"></div>
    </div>
  </div> 
  <script src="/js/prac06.js"></script>
  </body>
</html>
