<?php
//require '../../app/routes/config.php';
require '../../app/vendor/autoload.php';
$app = new \Slim\Slim();
$app->config(array(
    'templates.path' => '../../app/templates/'
));
$app->container->singleton('db', function () {
  return new PDO("pgsql:host=148.208.228.139 port=1111 user=a10160860 password=T3mp0r4l dbname=a10160860");
});

require '../../app/routes/root.php';
require '../../app/routes/xml.php';

$app->run();
